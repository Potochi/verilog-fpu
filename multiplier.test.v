// Using includes is recommended, as you can pass
// single files to the compiler, simplifying the
// build step.
// Not using these requires passing all needed files
// to the compiler manually
`include "./multiplier.v"

// Set the simulation timescale
`timescale 1 ns/10 ps

// Test fixture module
module TesterModule;
   reg [31:0] leftOp;
   reg [31:0] rightOp;
   wire [31:0] result;

   localparam period = 20;

   // Instantiate the module you want to test
   Multiplier main(
            .iw_LeftOperand(leftOp),
            .iw_RightOperand(rightOp),
            .ow_Result(result)
            );

   // This initial block will be executed only once
   initial begin
      // A change in inputs causes a change in outupts.
      // This can be viewed with gtkwave
      leftOp  = 32'h40a00000;
      rightOp = 32'h40a00000;
      #period;

      #period;
      leftOp  = 32'h20490FDB;
      rightOp = 32'h20490FDB;
      #period;

      rightOp = 0;

      #period;
      #period;

      // Forcefully end the simulation,
      // as cvc will not end it automagically when it
      // no longer detects changes
      $finish;
   end
endmodule
