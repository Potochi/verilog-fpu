FROM --platform=amd64 archlinux:latest

# Update the system to the latest version
RUN pacman -Syyuu --noconfirm

# Install packages that are needed for building 
RUN pacman -S --noconfirm git sudo fakeroot binutils patch make gcc

# Clone the CVC package repo
RUN git clone https://aur.archlinux.org/oss-cvc-git.git /cvc

# Make /cvc world write-able
RUN chmod 777 /cvc

# CD into the CVC directory
WORKDIR /cvc

# Create a builder user as makepkg cannot be run as root
RUN useradd -m -s /bin/sh builder

# Allow the build user to run pacman as root
RUN echo "builder ALL = NOPASSWD: /usr/bin/pacman" >> /etc/sudoers

# Change to the builder user
USER builder

# Create the package
RUN makepkg -s --noconfirm

# Change back to the root user
USER root

# Install the package
RUN pacman -U --noconfirm *.pkg*

WORKDIR /app

ENTRYPOINT [ "make" ]
