`include "./sp_type.v"

module Multiplier (
                   input wire [31:0]  iw_LeftOperand,
                   input wire [31:0]  iw_RightOperand,
                   output wire [31:0] ow_Result
                   );

   // Signals for the left operand
   wire                               leftOpSign;
   wire [7:0]                         leftOpExp;
   wire [22:0]                        leftOpMantissa;

   wire                               leftqNan;
   wire                               leftsNan;
   wire                               leftInfinity;
   wire                               leftNormal;
   wire                               leftSubnormal;
   wire                               leftZero;

   sp_type ltype(
                 .fp_Value(iw_LeftOperand),
                 .Sign(leftOpSign),
                 .Exponent(leftOpExp),
                 .Significant(leftOpMantissa),
                 .q_Nan(leftqNan),
                 .s_Nan(leftsNan),
                 .Infinity(leftInfinity),
                 .Normal(leftNormal),
                 .Subnormal(leftSubnormal),
                 .Zero(leftZero));


   // Signals for the right operand
   wire                               rightOpSign;
   wire [7:0]                         rightOpExp;
   wire [22:0]                        rightOpMantissa;

   wire                               rightqNan;
   wire                               rightsNan;
   wire                               rightInfinity;
   wire                               rightNormal;
   wire                               rightSubnormal;
   wire                               rightZero;

   sp_type rtype(
                 .fp_Value(iw_RightOperand),
                 .Sign(rightOpSign),
                 .Exponent(rightOpExp),
                 .Significant(rightOpMantissa),
                 .q_Nan(rightqNan),
                 .s_Nan(rightsNan),
                 .Infinity(rightInfinity),
                 .Normal(rightNormal),
                 .Subnormal(rightSubnormal),
                 .Zero(rightZero));

   // Registers that will be composed into the result
   // Sign of the result
   reg                                resSign;

   // Exponent of the result
   reg [ 9:0]                         resExp;

   // Mantissa of the result and temporary register
   // used for multiplication
   reg [47:0]                         mulRegFrac;

   // Result of the operation
   assign ow_Result = {resSign, resExp[7:0], mulRegFrac[45:23]};

   // Temporary storage
   reg [23:0]                         normRightSignificant;
   reg [23:0]                         normLeftSignificant;

   // If any of the operands change
   always @(iw_LeftOperand, iw_RightOperand) begin
      // Check if we are dividing by 0
      if (rightZero) begin
         // If we are dividing by 0, set the result to NaN
         mulRegFrac = 48'b0;
         resExp = 8'b11111111;
         resSign = 1'b0;
      end else if (leftsNan || leftqNan || rightsNan || leftsNan) begin
         // If we are operating on a NaN, we propagate it to the output.
         // Note: Here no distinction is made between quiet and signaling
         // NaN's because there is nothing to handle the exceptions, so
         // all NaN's are treated as quiet.
         if (leftsNan || leftqNan) begin
            {resSign, resExp[7:0], mulRegFrac[45:23]} = iw_LeftOperand;
         end else begin
            {resSign, resExp[7:0], mulRegFrac[45:23]} = iw_RightOperand;
         end
      end else if ((leftInfinity && rightZero) || (rightInfinity && leftZero)) begin
         // Handle the inf * 0 or 0 * inf case, which should output a signaling NaN
         resSign = 1'b0;
         resExp = 8'b11111111;
         mulRegFrac[45] = 1'b1;
         mulRegFrac[44:23] = 22'b101;
      end else if ((leftInfinity && (rightNormal || rightSubnormal))
                   || (rightInfinity && (leftNormal || leftSubnormal))
                   || (rightInfinity && leftInfinity)) begin
         // Handle the inf * x (or x * inf or inf * inf) case, where we should only
         // change the sign of the infinity
         resSign = leftOpSign ^ rightOpSign;
         resExp = 8'b11111111;
         mulRegFrac = 23'b0;
      end else if ((leftZero && (rightNormal || rightSubnormal))
                   || (rightZero && (leftNormal || leftSubnormal))
                   || (leftSubnormal && rightSubnormal)) begin
         // Handle the cases 0 * x, x * 0, sub * sub
         // Multiplying 2 subnormal/denormalized numbers will always
         // round down to 0
         resSign = leftOpSign ^ rightOpSign;
         resExp = 0;
         mulRegFrac = 0;
      end else if ((leftNormal || leftSubnormal) && (rightNormal || rightSubnormal)) begin
         // If we are not in an error case, concatenate
         // the hidden bits in the mantissa
         mulRegFrac = {leftNormal ? 1'b1 : 1'b0, iw_LeftOperand[22:0]} *
                      {rightNormal ? 1'b1 : 1'b0, iw_RightOperand[22:0]};

         resSign = leftOpSign ^ rightOpSign;

         // Compute the exponent and subtract one bias,
         // this is due to the fact that both the left
         // and right exponent already contain a bias
         // and we only need one for the result
         resExp = leftOpExp + rightOpExp - 9'h7f;

         // Normalize the result
         if (mulRegFrac[47] == 1) begin
            mulRegFrac = mulRegFrac >> 1;
            resExp = resExp + 1;
         end

         // If we happen to underflow the exponent (basically
         // generating a subnormal number), shift the mantissa
         // until the exponent becomes 0
         if (resExp[9] == 1) begin
            while (resExp != 0) begin
               mulRegFrac = mulRegFrac >> 1;
               resExp = resExp + 1;
            end

            // By the time we end up here the exponent is -127, but
            // the smallest representible exponent is -126, so we
            // need to shift this one last time.
            mulRegFrac = mulRegFrac >> 1;
         end else if (resExp[8] == 1) begin // if (resExp[9] == 1)
            // If we did not underflow and the result does not fit in 8 bits,
            // it means we have an infinity
            resExp = 10'b0011111111;
            mulRegFrac = 0;
         end
      end else begin
         mulRegFrac = 48'bx;
         resExp = 8'bx;
         resSign = 1'bx;
      end
   end
endmodule
