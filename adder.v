`include "./sp_type.v"

module Adder(
             input wire [31:0]  iw_LeftOperand,
             input wire [31:0]  iw_RightOperand,
             output wire [31:0] ow_Result
             );
   // Signals for the left operand
   wire                         leftOpSign;
   wire [7:0]                   leftOpExp;
   wire [22:0]                  leftOpMantissa;

   wire                         leftqNan;
   wire                         leftsNan;
   wire                         leftInfinity;
   wire                         leftNormal;
   wire                         leftSubnormal;
   wire                         leftZero;

   sp_type ltype(
                 .fp_Value(iw_LeftOperand),
                 .Sign(leftOpSign),
                 .Exponent(leftOpExp),
                 .Significant(leftOpMantissa),
                 .q_Nan(leftqNan),
                 .s_Nan(leftsNan),
                 .Infinity(leftInfinity),
                 .Normal(leftNormal),
                 .Subnormal(leftSubnormal),
                 .Zero(leftZero));


   // Signals for the right operand
   wire                         rightOpSign;
   wire [7:0]                   rightOpExp;
   wire [22:0]                  rightOpMantissa;

   wire                         rightqNan;
   wire                         rightsNan;
   wire                         rightInfinity;
   wire                         rightNormal;
   wire                         rightSubnormal;
   wire                         rightZero;

   sp_type rtype(
                 .fp_Value(iw_RightOperand),
                 .Sign(rightOpSign),
                 .Exponent(rightOpExp),
                 .Significant(rightOpMantissa),
                 .q_Nan(rightqNan),
                 .s_Nan(rightsNan),
                 .Infinity(rightInfinity),
                 .Normal(rightNormal),
                 .Subnormal(rightSubnormal),
                 .Zero(rightZero));

   // Registers that will be composed into the result
   // Sign of the result
   reg                          resSign;

   // Exponent of the result
   reg [ 8:0]                   resExp;

   // Result significant
   reg [24:0]                   resMantissa;

   // Result of the operation
   assign ow_Result = {resSign, resExp[7:0], resMantissa[22:0]};

   // Temporary storage
   reg [23:0]                   normRightSignificant;
   reg [23:0]                   normLeftSignificant;

   reg [8:0]                    tmpLeftExp;
   reg [8:0]                    tmpRightExp;

   // If any of the operands change
   always @(iw_LeftOperand, iw_RightOperand) begin
      if (leftsNan || leftqNan || rightsNan || leftsNan) begin
         // If we are operating on a NaN, we propagate it to the output.
         // Note: Here no distinction is made between quiet and signaling
         // NaN's because there is nothing to handle the exceptions, so
         // all NaN's are treated as quiet.
         if (leftsNan || leftqNan) begin
            {resSign, resExp[7:0], resMantissa[22:0]} = iw_LeftOperand;
         end else begin
            {resSign, resExp[7:0], resMantissa[22:0]} = iw_RightOperand;
         end
      end else if (leftZero && rightZero) begin // if (leftsNan || leftqNan || rightsNan || leftsNan)
         {resSign, resExp[7:0], resMantissa[22:0]} = iw_LeftOperand;
      end else if (leftZero && (rightNormal || rightSubnormal)) begin
         {resSign, resExp[7:0], resMantissa[22:0]} = iw_RightOperand;
      end else if (rightZero && (leftNormal || leftSubnormal)) begin
         {resSign, resExp[7:0], resMantissa[22:0]} = iw_LeftOperand;
      end else if (leftInfinity && rightInfinity && (leftOpSign ^ rightOpSign)) begin
         // Adding infinities with different signs is undefined, we output
         // a quiet NaN
         resSign = 1'b0;
         resExp = 8'b11111111;
         resMantissa[22] = 1'b0;
         resMantissa[21:0] = 22'b1011;
      end else if (((leftInfinity && (rightNormal || rightSubnormal))
                    || (rightInfinity && (leftNormal || leftSubnormal))
                    ) && (leftOpSign == rightOpSign) ) begin
         // Adding to an infinity with the same sign outputs infinity
         if (leftInfinity) begin
            {resSign, resExp[7:0], resMantissa[22:0]} = iw_LeftOperand;
         end else begin
            {resSign, resExp[7:0], resMantissa[22:0]} = iw_RightOperand;
         end
      end else if ((leftNormal || leftSubnormal) && (rightNormal || rightSubnormal)) begin
         // Append the implicit bit (1 for normal and 0 for subnormal numbers)
         normLeftSignificant = {leftNormal ? 1'b1 : 1'b0, leftOpMantissa};
         normRightSignificant = {rightNormal ? 1'b1 : 1'b0, rightOpMantissa};

         // Save the exponents into temporary registers because they need to be
         // adjusted (both numbers require to be the same exponent to perform the
         // addition)
         tmpRightExp = rightOpExp;
         tmpLeftExp = leftOpExp;

         // Change the representation of the inputs such that they
         // both have the same exponent
         while (tmpLeftExp > tmpRightExp) begin
            tmpRightExp = tmpRightExp + 1;
            // The values 0 and 1 encode the same exponent -126,
            // so we do not shift if we changed the exponent from a 0 to a 1
            if (tmpRightExp != 1) begin
               normRightSignificant = normRightSignificant >> 1;
            end
         end

         while (tmpRightExp > tmpLeftExp) begin
            tmpLeftExp = tmpLeftExp + 1;
            // The values 0 and 1 encode the same exponent -126,
            // so we do not shift if we changed the exponent from a 0 to a 1
            if (tmpLeftExp != 1) begin
               normLeftSignificant = normLeftSignificant >> 1;
            end
         end

         if ((leftOpSign ^ rightOpSign) == 0) begin
            // If both signs are the same, we basically perform an addition
            // and set the sign to be the same as the inputs
            resMantissa = normLeftSignificant + normRightSignificant;

            // Normalize the result
            if (resMantissa[24] == 1) begin
               resMantissa = resMantissa >> 1;
               resExp = tmpRightExp + 1;
            end else begin
               resExp = tmpRightExp;
            end

            resSign = leftOpSign;
         end else if ((leftOpSign ^ rightOpSign) == 1) begin // if ((leftOpSign == 0) && (rightOpSign == 0))
            // NOTE: Not sure about how signs are handled if the numbers
            // are equal

            // Compute the resulting sign based on the signs of the operands
            // and their significants and compute the resulting mantissa
            if (leftOpSign == 1 && rightOpSign == 0) begin
               if (normLeftSignificant > normRightSignificant) begin
                  resSign = 1;
               end else begin
                  resSign = 0;
               end

               resMantissa = normRightSignificant - normLeftSignificant;
            end else if (leftOpSign == 0 && rightOpSign == 1) begin
               if (normLeftSignificant > normRightSignificant) begin
                  resSign = 0;
               end else begin
                  resSign = 1;
               end
               resMantissa = normLeftSignificant - normRightSignificant;
            end

            // If we underflowed, get the two's complement of the mantissa
            if (resMantissa[24] == 1) begin
               resMantissa = (~resMantissa) + 1;
            end

            // Normalize the mantissa, by requiring a 1 on bit 23
            while ((resMantissa[23] == 0)
                   && (resMantissa[22:0] != 0)) begin
               resMantissa = resMantissa << 1;
               tmpRightExp = tmpRightExp - 1;
            end

            // If we detect an underflow in the exponent, we shift the mantissa
            // left until the exponent becomes 0
            if (tmpRightExp[8] == 1) begin
               while (tmpRightExp != 0) begin
                  resMantissa = resMantissa >> 1;
                  tmpRightExp = tmpRightExp + 1;
               end

               // By the time we end up here the exponent is -127, but
               // the smallest representible exponent is -126, so we
               // need to shift this one last time.
               resMantissa = resMantissa >> 1;
            end

            resExp = tmpRightExp;
         end

      end else begin // if (leftNormal && rightNormal)
         // Error out
         resMantissa[22:0] = 23'bx;
         resExp = 8'bx;
         resSign = 1'bx;
      end
   end // always @ (iw_LeftOperand, iw_RightOperand)
endmodule // Adder
