module sp_type
     #(
       parameter EXPONENT_BITS=8,
       parameter MANTISSA_BITS=23,
       parameter SIGN_BITS=1)
   (
    input wire [SIGN_BITS+EXPONENT_BITS+MANTISSA_BITS-1:0] fp_Value,
    output wire [SIGN_BITS-1:0]                            Sign,
    output wire [EXPONENT_BITS-1:0]                        Exponent,
    output wire [MANTISSA_BITS-1:0]                        Significant,
    output wire                                            q_Nan,
    output wire                                            s_Nan,
    output wire                                            Infinity,
    output wire                                            Normal,
    output wire                                            Subnormal,
    output wire                                            Zero);

   assign Sign
     = fp_Value[MANTISSA_BITS+EXPONENT_BITS+SIGN_BITS-1:MANTISSA_BITS+EXPONENT_BITS];

   assign Exponent
     = fp_Value[MANTISSA_BITS+EXPONENT_BITS-1:MANTISSA_BITS];

   assign Significant
     = fp_Value[MANTISSA_BITS-1:0];


   assign q_Nan = 
                  fp_Value[MANTISSA_BITS-1] == 1 &&
                  fp_Value[MANTISSA_BITS-2:0] != 0 &&
                  fp_Value[MANTISSA_BITS+EXPONENT_BITS-1:MANTISSA_BITS] == ~0;

   assign s_Nan =
                 fp_Value[MANTISSA_BITS-1] == 0 &&
                 fp_Value[MANTISSA_BITS-2:0] != 0 &&
                 fp_Value[MANTISSA_BITS+EXPONENT_BITS-1:MANTISSA_BITS] == ~0;

   assign Infinity =
                    fp_Value[MANTISSA_BITS-1] == 0 &&
                    fp_Value[MANTISSA_BITS+EXPONENT_BITS-1:MANTISSA_BITS] == ~0;

   assign Subnormal = fp_Value[MANTISSA_BITS-1:0] != 0 &&
                      fp_Value[MANTISSA_BITS+EXPONENT_BITS-1:MANTISSA_BITS] == 0;

   assign Zero = fp_Value[MANTISSA_BITS-1:0] == 0 &&
                 fp_Value[MANTISSA_BITS+EXPONENT_BITS-1:MANTISSA_BITS] == 0;

   assign Normal = !(q_Nan || s_Nan || Infinity || Subnormal || Zero);

endmodule
