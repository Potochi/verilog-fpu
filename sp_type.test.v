// Using includes is recommended, as you can pass
// single files to the compiler, simplifying the
// build step.
// Not using these requires passing all needed files
// to the compiler manually
`include "./sp_type.v"

// Set the simulation timescale
`timescale 1 ns/10 ps

// Test fixture module
module SpTypeTestModule;
   reg [31:0] fp;
   wire       qnan, snan, infi, zero, subnorm, norm;

   localparam period = 20;

   // Instantiate the module you want to test
   sp_type main(
                .fp_Value(fp),
                .q_Nan(qnan),
                .s_Nan(snan),
                .Infinity(infi),
                .Normal(norm),
                .Subnormal(subnorm),
                .Zero(zero)
            );

   // This initial block will be executed only once
   initial begin
      // A change in inputs causes a change in outupts.
      // This can be viewed with gtkwave
      fp = 32'h40a00000;
      #period;

      fp = 32'h0;
      #period;

      // Forcefully end the simulation,
      // as cvc will not end it automagically when it
      // no longer detects changes
      $finish;
   end
endmodule
