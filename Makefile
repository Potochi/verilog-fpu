CVCFLAGS=+fstvars
CVCCC=cvc64

# Example command that runs the simulation and produces
# the wave file
simulate: adder.cvcbin
	./$<

sp_type: sp_type.cvcbin
	./$<

# Open wave file in gtkwave
gtkwave: verilog.fst
	gtkwave $<

# Build a binary from a test fixture
%.cvcbin: %.test.v %.v
	$(CVCCC) $(CVCFLAGS) -o $@ $<
